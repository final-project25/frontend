FROM openjdk:8-alpine AS build

RUN apk update

# Install nodejs, yarn and create project directory
ENV PATH ${PATH}:/usr/local/node-v14.15.0-linux-x64-musl/bin
WORKDIR /usr/local
RUN wget https://unofficial-builds.nodejs.org/download/release/v14.15.0/node-v14.15.0-linux-x64-musl.tar.xz && \
    tar -xvf node-v14.15.0-linux-x64-musl.tar.xz && \
    rm -rf node-v14.15.0-linux-x64-musl.tar.xz && \
    npm install --global yarn && \
    mkdir ~/frontend

COPY . /frontend
WORKDIR /frontend
RUN chmod +x gradlew
RUN ./gradlew jar


FROM openjdk:8-jdk-alpine AS app
COPY --from=build /frontend/devschool-front-app-server/build/libs/*jar .
EXPOSE 8081
ENTRYPOINT ["java","-jar","devschool-front-app-server-1.0.0.jar","-P:ktor.backend.host=backend-service"]
